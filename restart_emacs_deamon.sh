#!/usr/bin/env sh

command=$(ps -aux | grep /usr/bin/emacs)
pid_d=$(pidof /usr/bin/emacs)
pid_e=$(pidof emacsclient)

if ( pidof /usr/bin/emacs )
then
    echo killing emacs daemon
    $(sudo kill -9 $pid_d)
    echo starting up emacs daemon
    $(/usr/bin/emacs --daemon &)
    echo starting emacsclient
    $(emacsclient -c -a 'vim')
    exit 0
else
    echo daemon not running. starting.
    $(/usr/bin/emacs --daemon &)
    echo starting emacsclient
    $(emacsclient -c -a 'vim' &)
fi

exit 0
